<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App\Api;

use Doctrine\Common\Collections\ArrayCollection;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;

use function json_decode;

class FilmApiDataLoader
{
    protected Client $filmApiClient;

    protected FilesystemAdapter $cache;

    public function __construct(Client $filmApiClient, FilesystemAdapter $cache)
    {
        $this->filmApiClient = $filmApiClient;
        $this->cache = $cache;
    }

    public function loadData(): ArrayCollection
    {
        $data = $this->cache->get("titles", function () {
            /* @var $response ResponseInterface */
            $response = $this->filmApiClient->send($this->filmApiClient->getRequest('items'));

            return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        });

        return new ArrayCollection($data);
    }

    public function loadItemData(int $id)
    {
        return $this->cache->get("title_$id", function () use ($id) {
            $response = $this->filmApiClient->send($this->filmApiClient->getRequest("items/$id"));

            return json_decode($response->getBody()->getContents(), true, 512, JSON_THROW_ON_ERROR);
        });
    }
}
