<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App\Core;

use Loopia\App\Error\MethodNotAllowedException;
use Loopia\App\Error\NotFoundException;

class FilmApiHttpApplication extends HttpApplication
{
    /**
     * @throws NotFoundException
     * @throws MethodNotAllowedException
     */
    public function run(): void
    {
        $content = parent::run();
        \header('Content-Encoding: utf-8');
        \header('X-Data: films');
        \header('Content-Length: ' . \strlen($content));
        \header('Cache-Control: max-age=3600, must-revalidate');
        \header('Expires: ' . gmdate('D, d M Y H:i:s \G\M\T', time() + 3600));

        echo $content;
    }
}
