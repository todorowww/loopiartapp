<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App\Template;

use Exception;

class Template
{
    protected string $templatePath;
    protected array $vars;

    public function __construct(string $templatePath, array $vars = [])
    {
        $this->templatePath = $templatePath;
        $this->vars = $vars;
    }


    /**
     * @throws Exception
     */
    public function __get($name)
    {
        if (!array_key_exists($name, $this->vars)) {
            throw new Exception('Template doesn\'t have variable: ' . $name);
        }

        return $this->vars[$name];
    }

    public function render(): bool|string
    {
 //preimenuj funkciju, dodaj još nešto, za merge conlict
        ob_start();
        include $this->templatePath;
        return ob_get_clean();
    }
}
