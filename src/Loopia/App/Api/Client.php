<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App\Api;

use Closure;
use Exception;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\ResponseInterface;

class Client implements CredentialsConsumerInterface
{
    protected Credentials $credentials;
    protected GuzzleClient $client;

    public function __construct(Credentials $credentials, GuzzleClient $client)
    {
        $this->credentials = $credentials;
        $this->client = $client;
    }

    public function getConsumerClosure(): Closure
    {
        return function () {
            /* @var $this Credentials */
            if (false !== $hash = password_hash($this->getPassword(), PASSWORD_DEFAULT)) {
                return $this->getUsername() . ':' . base64_encode($hash);
            }

            throw new Exception('Failed creating authentication hash');
        };
    }

    public function getRequest(string $uri): Request
    {
        return new Request('GET', $uri, [
            'X-Authorization' => 'Bearer ' . $this->credentials->visit($this),
            'Accept' => 'application/json',
            'Cache-Control' => 'no-cache'
        ]);
    }

    public function send(Request $request): ResponseInterface
    {
        return $this->client->send($request);
    }
}
