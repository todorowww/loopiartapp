### Backend API doesn't seem to show the most recent results. Is there any caching on the backend? How do you know? What can you do to get fresh results?
The clue was in the response headers, `Cache-Control` should be set in order to show fresh data.

### Suggest any way in which the application can be improved
The main point would be to implement caching, which was done as part of a task.

API response content could be translated to proper objects, instead of using associative array. 

### DNS related questions

#### Does host wrydeedkog.loopiarnd.com have any TXT records?
Yes. `"rectory roofers their ladders take them higher"`

#### Which email server should receive email sent to email@wrydeedkog.loopiarnd.com?
`ralfuvlulb.ivcydsewip.dev` server will receive emails
