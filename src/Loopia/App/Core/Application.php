<?php

/*
 * © Loopia. All rights reserved.
 */

namespace Loopia\App\Core;

use Psr\Log\LoggerInterface;

abstract class Application
{
    protected LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }
}
